﻿
$profDir = Split-Path $profile;

if (-not (Test-Path $profDir)) {
  mkdir $profDir
}

if (-not (Test-Path $profile)) {
  Set-Content -Value "" -Path $profile
}

$installDir = split-path -parent $MyInvocation.MyCommand.Definition
$content = Get-Content $profile
$content += "`n . $($installDir)\playn-setup-env.ps1"

Set-Content -Value $content -Path $profile
