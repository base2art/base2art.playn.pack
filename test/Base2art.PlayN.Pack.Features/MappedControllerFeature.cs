﻿namespace Base2art.PlayN.Pack.Features
{
    using System;

    using Base2art.PlayN.Api;
    using Base2art.PlayN.Api.Diagnostics;
    using Base2art.PlayN.Http;
    using Base2art.PlayN.Http.Owin;
    using Base2art.PlayN.Mvc;
    using Base2art.PlayN.Pack.Features.Fixtures;

    using FluentAssertions;

    using Microsoft.Owin;

    using NUnit.Framework;

    [TestFixture]
    public class MappedControllerFeature
    {
        [Test]
        public void ShouldLoadController()
        {
            var router = FakeRouter.Create(typeof(MappedController).GetClass().As<IRenderingController>());
            var a = new ControllerExecutionManager(new Application(ApplicationMode.Prod,Environment.CurrentDirectory, null), router);
            IResult rezult = a.ExecuteController(this.CreateContext(a.Application));
            rezult.Content.Body.AsString().Should().Be("Content For @{MonkeyTail}");
        }

        private IHttpContext CreateContext(IApplication application)
        {
            return new HttpContext(application, new NullLogger(), null, new OwinContext(), new HttpContextSettings());
        }
    }
}