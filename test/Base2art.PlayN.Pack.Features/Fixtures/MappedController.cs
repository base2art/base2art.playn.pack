﻿namespace Base2art.PlayN.Pack.Features.Fixtures
{
    using System.Collections.Generic;
    using System.Text;

    using Base2art.PlayN.Api;
    using Base2art.PlayN.Http;
    using Base2art.PlayN.Mvc;

    public class MappedController : SimpleRenderingController
    {
        protected override IResult ExecuteMain(IHttpContext httpContext, List<PositionedResult> childResults)
        {
            var content = new MonkeyTail.ContentTypes.Html(new StringBuilder("Content For @{MonkeyTail}"));
            return httpContext.Ok(content);
        }
    }
}
