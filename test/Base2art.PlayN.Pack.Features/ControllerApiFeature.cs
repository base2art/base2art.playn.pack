﻿
namespace Base2art.PlayN.Pack.Features
{
    using System.Collections.Generic;
    using System.Diagnostics;
    using Base2art.MonkeyTail.Api;
    using Base2art.PlayN.Api;
    using Base2art.PlayN.Api.Routing.Functional;
    using Base2art.PlayN.Http;
    using Base2art.PlayN.Mvc;
    
    public class ControllerApiFeature : SimpleRenderingController
    {
        protected override IResult ExecuteMain(IHttpContext httpContext, List<PositionedResult> childResults)
        {
            return httpContext.NoContent();
        }
        
        protected override IEnumerable<IPositionedRenderingController> RenderingControllers
        {
            get
            {
                yield return this.CreateRenderingController(BoxModelGuidePost.BottomBottom, 1, new NullRenderingController());
                yield return this.CreateRenderingController(BoxModelGuidePost.BottomBottom, 1, (x, y) => x.Ok(new SimpleContent{ BodyContent = "String" }));
            }
        }
        
        protected override IEnumerable<INonRenderingController> NonRenderingControllers
        {
            get
            {
                yield return this.CreateNonRenderingController(x => Debug.WriteLine(x.ToString()));
            }
        }
    }
}
