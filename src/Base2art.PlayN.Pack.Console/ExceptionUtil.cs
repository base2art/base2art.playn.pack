namespace Base2art.PlayN.Pack
{
    using System;

    public class ExceptionUtil
    {
        public static T Retry<T>(int tries, Func<T> func, int counter = 0)
        {
            try
            {
                return func();
            }
            catch (Exception)
            {
                if (counter >= tries)
                {
                    throw;
                }

                return Retry<T>(tries, func, counter +1);
            }
        }
    }
}