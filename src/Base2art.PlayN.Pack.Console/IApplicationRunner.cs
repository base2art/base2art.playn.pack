﻿namespace Base2art.PlayN.Pack
{
    using System;

    public interface IApplicationRunner : IDisposable
    {
        void Run(
            string rootDir,
            string binPath,
            string currentDomainBin,
            int? portNumber);
    }
}