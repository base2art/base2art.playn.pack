﻿
$installDir = split-path -parent $MyInvocation.MyCommand.Definition

$pathAdd = Join-Path $installDir "playn"

if (Test-Path $pathAdd) {
  $env:Path += ";$($pathAdd)"
}

$pathAdd = Join-Path $installDir "src\Base2art.PlayN.Pack.Console\bin\Debug"

if (Test-Path $pathAdd) {
  $env:Path += ";$($pathAdd)"
}

